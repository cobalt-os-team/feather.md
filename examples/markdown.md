# Le markdown :

## Utilité :
* Documentation (Utilisé par github par exemple)
* Faire des articles
* Utilisable partout
* Frappe efficace

## Utilisation :
### Sauvegarder un fichier :
Pour sauvegarder un fichier MarkDown, il suffit de l'enrgistrer à l'emplacement souhaité avec l'extension ".md".

### Paragraphes :
Ont peut séparer les paragraphes en sotant des lignes. Voici donc un exemple d'un paragraphe que j'ai écrit en MarkDown dans GhostWriter qui est un logiciel libre qui permet d'éditer du texte en MarkDown.

Vous avez donc le résultat de ce que cela donne. Et je pense m'en servir pour faire de la prise de notes au lycée sur mon ordinateur.

### Listes :
Voici différents types de listes :

#### Les listes non-ordonnées (sans nombres) :

* Ligne A
* Ligne B
* Ligne C

#### Listes ordonnées (avec des nombres):

1. Ligne 1
2. Ligne 2
3. Ligne 3

### Texte en GRAS :
**Morceau de texte en GRAS.**

### Texte en *Italique* :
*Voici dun texte en italique.*
_Ou encore._

### Hyperliens :
Voici un morceau de texte contenant un [HyperLien](https://linuxtricks.fr "LinuxTricks") qui mène au site LinuxTricks.

### Insérer des Images :
Voici une image ![logo linuxtricks](https://www.linuxtricks.fr/templates/linuxtricks/theme/images/logo.png) simple provenant du net.

### Citations :
> Le mot juste aiguise la pensée.

### Insertion de code :
	print("Hello, World");

### Parser ces fichiers MarkDown :
Si vous voulez parser vos fichier markdown voici un bon outil : [https://www.markdowntopdf.com/](https://www.markdowntopdf.com/)

Ou encore un autre en ligne de commande :
[https://pandoc.org/index.html](https://pandoc.org/index.html)

#### Utilisation de Pandoc :

Pandoc est un outils bien pratique en ligne de commande pour parser ces fichiers MarkDown (et même bien d'autres formats).

##### Installation sous MS Windows :

On installe d'abord le paquet "pandoc" :

	choco install pandoc
Puis on installe les dépendances :

	choco install rsvg-convert python miktex

##### Installation sous GNU/Linux :

L'installation de Pandoc est différente en fonction de votre distribution Linux.

##### Utilisation :

	pandoc -f <format_1> -t <format_2> -o <fichier_sortant>

## Conclusion :
Le MarkDown est un formatage très intéressant, et puissant, que je risque probablement d'utiliser pour faire de la prise de notes rapides pendants mes cours afin d'être plus éfficace pour l'apprentissage des connaissances.