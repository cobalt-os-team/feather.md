const { app, BrowserWindow, Menu, MenuItem, nativeImage, dialog } = require("electron")
const ipc = require("electron").ipcMain;
const exists = require("fs").existsSync
const { writeFile, mkdir } = require("fs")

let fileToOpen; //macOS only

const isMac = process.platform === "darwin"
const dataFolder = process.env.APPDATA || `${process.env.HOME}/${isMac ? '/Library/Preferences' : '/.local/share'}`
app.name = "Feather.md";

let config = {
  selectedTheme: 'default',
  selectedEditorTheme: 'default',
  selectedPreviewTheme: 'default',
  recent: []
};
let mainWindow = null
let template = []

function initialize() {
  makeSingleInstance();

  function createWindow() {

    template = [
      ...(isMac
        ? [
            {
              label: app.name,
              submenu: [
                { role: "about" },
                { type: "separator" },
                { role: "services" },
                { type: "separator" },
                { role: "hide" },
                { role: "hideothers" },
                { role: "unhide" },
                { type: "separator" },
                { role: "quit" },
              ],
            },
          ]
        : []),
      {
        label: "&File",
        submenu: [
          {
            label: "New File",
            accelerator: "CmdOrCtrl+N",
            click: () => {
              mainWindow.webContents.send("new-tab");
            },
          },
          {
            label: "Open File...",
            accelerator: "CmdOrCtrl+O",
            click: () => {
              dialog
                .showOpenDialog(mainWindow, {
                  properties: ["openFile", "multiSelections"],
                  filters: [
                    { name: "Markdown", extensions: ["md"] },
                    { name: "Text", extensions: ["txt"] },
                  ]
                })
                .then((result) => {
                  if (result.filePaths.length)
                    addRecentFiles(result.filePaths)
                    result.filePaths.forEach((path) => {
                      mainWindow.webContents.send("file-to-open", path)
                    });
                })
                .catch((err) => {
                  console.log(err);
                });
            },
          },
          {
            label: "Open Recent",
            id: "openrecent",
            submenu: [
              {
                label: "Clear Menu",
                click: () => {
                  config.recent = []
                  saveConfig()
                  addRecentFiles()
                }
              }
            ]
          },
          {
            label: "Save",
            accelerator: "CmdOrCtrl+S",
            click: () => {
              mainWindow.webContents.send("save-focused-tab", null);
            },
          },
          {
            label: "Save As...",
            accelerator: "CmdOrCtrl+Shift+S",
            click: () => {
              saveAs();
            },
          },
          {
            label: "Close File",
            accelerator: "CmdOrCtrl+W",
            click: () => {
              mainWindow.webContents.send("close-focused-tab");
            },
          },
        ],
      },
      {
        label: "&Edit",
        submenu: [
          { label: "Undo", role: "undo" },
          { label: "Redo", role: "redo" },
          { type: "separator" },
          { label: "Cut", role: "cut" },
          { label: "Copy", role: "copy" },
          { label: "Paste", role: "paste" },
          { label: "Delete", role: "delete" },
          { label: "Select All", role: "selectall" },
        ],
      },
      {
        label: "&View",
        submenu: [
          {
            label: "General theme",
            submenu: [
              {
                id: "general-default",
                label: "Default",
                type: "radio",
                checked: true,
                click: () => {
                  changeTheme("general", "default");
                },
              },
              {
                id: "general-dark",
                label: "Dark",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("general", "dark");
                },
              },
            ],
          },
          {
            label: "Editor theme",
            submenu: [
              { label: "-Recommended themes-", enabled: false },
              {
                id: "editor-default",
                label: "Default",
                type: "radio",
                checked: true,
                click: () => {
                  changeTheme("editor", "default");
                },
              },
              {
                id: "editor-pastel-on-dark",
                label: "pastel-on-dark",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "pastel-on-dark");
                },
              },
              {
                id: "editor-ambiance",
                label: "ambiance",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "ambiance");
                },
              },
              {
                id: "editor-eclipse",
                label: "eclipse",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "eclipse");
                },
              },
              {
                id: "editor-mdn-like",
                label: "mdn-like",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "mdn-like");
                },
              },
              {
                id: "editor-mbo",
                label: "mbo",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "mbo");
                },
              },
              {
                id: "editor-monokai",
                label: "monokai",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "monokai");
                },
              },
              {
                id: "editor-neat",
                label: "neat",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "neat");
                },
              },
              { label: "-Light-", enabled: false },
              {
                id: "editor-3024-day",
                label: "3024-day",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "3024-day");
                },
              },
              {
                id: "editor-base16-light",
                label: "base16-light",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "base16-light");
                },
              },
              {
                id: "editor-eclipse-1",
                label: "eclipse",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "eclipse");
                },
              },
              {
                id: "editor-elegant",
                label: "elegant",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "elegant");
                },
              },
              {
                id: "editor-mdn-like-1",
                label: "mdn-like",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "mdn-like");
                },
              },
              {
                id: "editor-neat-1",
                label: "neat",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "neat");
                },
              },
              {
                id: "editor-neo",
                label: "neo",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "neo");
                },
              },
              {
                id: "editor-paraiso-light",
                label: "paraiso-light",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "paraiso-light");
                },
              },
              {
                id: "editor-solarized",
                label: "solarized",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "solarized");
                },
              },
              {
                id: "editor-xq-light",
                label: "xq-light",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "xq-light");
                },
              },
              { label: "-Dark-", enabled: false },
              {
                id: "editor-3024-night",
                label: "3024-night",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "3024-night");
                },
              },
              {
                id: "editor-ambiance-1",
                label: "ambiance",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "ambiance");
                },
              },
              {
                id: "editor-base16-dark",
                label: "base16-dark",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "base16-dark");
                },
              },
              {
                id: "editor-blackboard",
                label: "blackboard",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "blackboard");
                },
              },
              {
                id: "editor-cobalt",
                label: "cobalt",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "cobalt");
                },
              },
              {
                id: "editor-erlang-dark",
                label: "erlang-dark",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "erlang-dark");
                },
              },
              {
                id: "editor-lesser-dark",
                label: "lesser-dark",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "lesser-dark");
                },
              },
              {
                id: "editor-mbo-1",
                label: "mbo",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "mbo");
                },
              },
              {
                id: "editor-midnight",
                label: "midnight",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "midnight");
                },
              },
              {
                id: "editor-monokai-1",
                label: "monokai",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "monokai");
                },
              },
              {
                id: "editor-night",
                label: "night",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "night");
                },
              },
              {
                id: "editor-paraiso-dark",
                label: "paraiso-dark",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "paraiso-dark");
                },
              },
              {
                id: "editor-pastel-on-dark-1",
                label: "pastel-on-dark",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "pastel-on-dark");
                },
              },
              {
                id: "editor-rubyblue",
                label: "rubyblue",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "rubyblue");
                },
              },
              {
                id: "editor-the-matrix",
                label: "the-matrix",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "the-matrix");
                },
              },
              {
                id: "editor-tomorrow-night-eighties",
                label: "tomorrow-night-eighties",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "tomorrow-night-eighties");
                },
              },
              {
                id: "editor-twiilight",
                label: "twilight",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "twilight");
                },
              },
              {
                id: "editor-vibrant-ink",
                label: "vibrant-ink",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "vibrant-ink");
                },
              },
              {
                id: "editor-xq-dark",
                label: "xq-dark",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("editor", "xq-dark");
                },
              },
            ],
          },
          {
            label: "Preview theme",
            submenu: [
              {
                id: "preview-default",
                label: "Default",
                type: "radio",
                checked: true,
                click: () => {
                  changeTheme("preview", "default");
                },
              },
              {
                id: "preview-dark",
                label: "Dark",
                type: "radio",
                checked: false,
                click: () => {
                  changeTheme("preview", "dark");
                },
              },
            ],
          },
          { type: "separator" },
          { label: "Reset Zoom", role: "resetzoom" },
          { label: "Zoom +", role: "zoomin" },
          { label: "Zoom -", role: "zoomout" },
          { type: "separator" },
          { label: "Full screen", role: "togglefullscreen" },
        ],
      },
    ];

    mainWindow = new BrowserWindow({
      icon: nativeImage.createFromPath('./src/images/icon.png'),
      webPreferences: {
        nodeIntegration: true,
        contextIsolation: false
      },
      height: 700,
      width: 1000,
      show: false,
      backgroundColor: '#939292'
    })
    Menu.setApplicationMenu(Menu.buildFromTemplate(template))

    reloadConfig().then(() => {
      if (isMac) {
        app.whenReady().then(() => {

          template[1].submenu[2] = {
            label: "Open Recent",
            role: "recentdocuments",
            submenu: [
              {
                label: "Clear Menu",
                role: "clearrecentdocuments"
              }
            ]
          }
          Menu.setApplicationMenu(Menu.buildFromTemplate(template))
          updateMenu()
        })
        return
      }
      addRecentFiles()
      updateMenu()
    })

    mainWindow.loadURL(`file://${__dirname}/index.html`)
    if (process.env.ENV == 'dev ') mainWindow.webContents.openDevTools()
    mainWindow.maximize()
    mainWindow.hasUnsavedFiles = false

    mainWindow.on('close', event => {
      if (mainWindow.hasUnsavedFiles) {
        let save = dialog.showMessageBoxSync({
          type: "warning",
          title: "Save?",
          message: 'Some files aren\'t saved, do you want to save them?',
          buttons: ["Yes", "No", "Cancel"],
        })
        if (save === 1) return
        event.preventDefault()
        mainWindow.webContents.send("save-all-files-reply", save)
      }
    })
    mainWindow.on("closed", function () {
      mainWindow = null;
    });

    mainWindow.once("ready-to-show", () => {
      mainWindow.show();
    });

    mainWindow.webContents.on("did-finish-load", () => {
      if (isMac) {
        if (fileToOpen) mainWindow.webContents.send("file-to-open", fileToOpen) || app.addRecentDocument(fileToOpen)
        fileToOpen = null;
      } else handleArguments(process.argv);
    });
  }
  app.on("will-finish-launching", () => {
    app.on("open-file", (event, path) => {
      event.preventDefault();
      if (mainWindow != null) {
        app.addRecentDocument(path)
        mainWindow.webContents.send("file-to-open", path);
        if (mainWindow.isMinimized()) mainWindow.restore();
        mainWindow.focus();
      } else fileToOpen = path;
    });
  });
  app.on("ready", createWindow);

  app.on("window-all-closed", function () {
    if (!isMac) app.quit()
  });

  app.on("activate", function () {
    if (mainWindow === null) createWindow();
  });
}

function makeSingleInstance() {
  if (process.mas) return;

  if (!app.requestSingleInstanceLock()) {
    app.quit();
  } else {
    app.on("second-instance", (event, argv) => {
      if (mainWindow) {
        handleArguments(argv);
        if (mainWindow.isMinimized()) mainWindow.restore();
        mainWindow.focus();
      }
    });
  }
}

async function reloadConfig() {
  if (!exists(`${dataFolder}/.feather.md`)) {
    mkdir(`${dataFolder}/.feather.md`, (err) => {
      if (err) {
        dialog.showErrorBox('Error', `Unable to create the feather's path, report the error below :\n${err}`)
      }else {
        writeFile(`${dataFolder}/.feather.md/config.json`, JSON.stringify(config), (err) => {
          if (err) {
            dialog.showErrorBox('Error', `Unable to create the config, report the error below :\n${err}`)
          }
        })
      }
    })
  }else if (!exists(`${dataFolder}/.feather.md/config.json`)) {
    writeFile(`${dataFolder}/.feather.md/config.json`, JSON.stringify(config), (err) => {
      if (err) {
        dialog.showErrorBox('Error', `Unable to create the config, report the error below :\n${err}`)
      }
    })
  }else config = require(`${dataFolder}/.feather.md/config`)
}

async function saveConfig() {
  if (!exists(`${dataFolder}/.feather.md`)) {
    mkdir(`${dataFolder}/.feather.md`, (err) => {
      if (err) {
        dialog.showErrorBox('Error', `Unable to create the feather's path, report the error below :\n${err}`)
      }else {
        writeFile(`${dataFolder}/.feather.md/config.json`, JSON.stringify(config), (err) => {
          if (err) {
            dialog.showErrorBox('Error', `Unable to save the config, report the error below :\n${err}`)
          }
        })
      }
    })
    
  }else {
    writeFile(`${dataFolder}/.feather.md/config.json`, JSON.stringify(config), (err) => {
      if (err) {
        dialog.showErrorBox('Error', `Unable to save the config, report the error below :\n${err}`)
      }
    })
  }
}

function updateMenu() {
  let menuItems = Menu.getApplicationMenu()
  menuItems.getMenuItemById(`general-${config.selectedTheme}`).checked = true
  menuItems.getMenuItemById(`editor-${config.selectedEditorTheme}`).checked = true
  menuItems.getMenuItemById(`preview-${config.selectedPreviewTheme}`).checked = true
}

async function addRecentFiles(paths) {
  if (paths) {
    if (paths instanceof Array === false) paths = [paths]
    paths.forEach(file => app.addRecentDocument(file))
    if (isMac) return

    config.recent = config.recent.filter((val, i) => !paths.includes(val) && i+paths.length < 11)
    config.recent.unshift(...paths)
    saveConfig()
  }
  let submenu = template[0].submenu[2].submenu
  submenu.splice(0, submenu.length - 1)

  config.recent.forEach(path => {
    submenu.splice(submenu.length - 1, 0, {
      label: path,
      click: () => {
        mainWindow.webContents.send("file-to-open", path)
        addRecentFiles(path)
      }
    })
  })
  submenu.splice(submenu.length - 1, 0, { type: "separator"} )
  Menu.setApplicationMenu(Menu.buildFromTemplate(template))
  updateMenu()
}

function handleArguments(argv) {
  if (argv.length >= 2 && argv[argv.length - 1] !== ".") {
    argv.slice(1).forEach(path => path != '.' && !path.startsWith('--') && (mainWindow.webContents.send("file-to-open", path) || addRecentFiles(path)))
  }
}

function saveAs(beforeClose, tabId) {
  const path = dialog.showSaveDialogSync(mainWindow, {
      filters: [
        { name: "Markdown", extensions: ["md"] },
        { name: "Text", extensions: ["txt"] },
      ]
  })
  if (path) {
    addRecentFiles(path)
    mainWindow.webContents.send(beforeClose ?
      "confirm-file-close" : "save-focused-tab", path, tabId)
  }
}

function changeTheme(area, theme) {
  mainWindow.webContents.send("theme-changed", area, theme);
  switch (area) {
    case "general":
      config.selectedTheme = theme;
      break;
    case "editor":
      config.selectedEditorTheme = theme
      break;
    case "preview":
      config.selectedPreviewTheme = theme
      break;
  }
  saveConfig()
}

ipc.on("save-as", (event, beforeClose, tabId) => {
  saveAs(beforeClose, tabId);
});

ipc.on("confirm-file-close", (event, message, tabId) => {
  dialog
    .showMessageBox({
      type: "warning",
      title: "Save?",
      message: message,
      buttons: ["Yes", "No", "Cancel"],
    })
    .then((result) => {
      mainWindow.webContents.send("confirm-file-close-reply", result.response, tabId);
    });
});
ipc.on("exit", () => {
  app.quit();
});
ipc.on("has-unsaved-files", (event, hasUnsavedFiles) => mainWindow.hasUnsavedFiles = hasUnsavedFiles)
ipc.on(("update-app-title"), (e, title) => mainWindow.title = `Feather.md${title ? ' - '+title : ''}`)

initialize();
