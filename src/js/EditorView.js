const EventEmitter = require('events').EventEmitter
const EditorTab = require("./EditorTab");
class EditorView extends EventEmitter {
    /**
     * @type {Array<EditorTab>}
     * @readonly
     */
    #tabs = [];
    /**
     * @type {EditorTab}
     */
    #focusedTab = null;
    #editor;
    #save = true;
    constructor() {
        super()
        this.#editor = editormd("editor", {
            path: "src/libs/meditorjs/lib/",
            pluginPath: "src/libs/meditorjs/plugins/",
            width: "100%",
            height: "100%",
            placeholder: "",
            codeFold: true,
            tocm: true,
            tocDropdown: true,
            taskList: true,
            emoji: true,
            tex: true,
            flowChart: true,
            sequenceDiagram: true,
            theme: localStorage.getItem("mdEditorThemeGeneral") || "default",
            editorTheme: localStorage.getItem("mdEditorThemeEditor") || "default",
            previewTheme: localStorage.getItem("mdEditorThemePreview") || "default",
            onchange: () => {
                if (this.#editor.getMarkdown() == null) return;
                if (!this.#save) {
                    this.#save = true
                    return;
                }
                if (this.#tabs.length >= 1) {
                    this.#focusedTab.updateContent(this.#editor.getMarkdown())
                    this.emit('content-updated', this.#editor.getMarkdown())
                }else {              
                    let tab = this.createTab(null)
                    tab.updateContent(this.#editor.getMarkdown())
                    this.#focusedTab = tab
                    this.emit('content-updated', this.#editor.getMarkdown())
                }
            }
        });
    }

    /**
     * Create a new tab return the tab object
     * @param {String} path the path to the file we want to display may be null
     * @returns {EditorTab}
     */
    createTab(path) {
        let tab = new EditorTab(path, this.#tabs.length)
        this.#tabs.push(tab)
        this.emit('tab-created', tab)
        return tab;
    }

    removeTab(tab) {
        let i = this.getTabIndex(tab)
        this.#tabs.splice(i, 1)
        if (this.#focusedTab === tab) {
            if (this.#tabs.length > 0) {
                if (i === this.#tabs.length) this.#focusedTab = this.#tabs[i-1]
                else this.#focusedTab = this.#tabs[i]
            }else this.#focusedTab = null;
        }
        this.#tabs.forEach((x, i) => x.setId(i))
        this.emit('tab-removed', tab, this.#focusedTab)
    }

    /**
     * 
     * @param {Number} id the id of tab to get
     * @returns {EditorTab}
     */
    getTab(id) { return this.#tabs[id] }

    /**
     * @returns {Array<EditorTab>}
     */
    getTabs() { return this.#tabs }

    getEditor() { return this.#editor }

    getFocusedTab() { return this.#focusedTab }

    getTabIndex(tab) { return this.#tabs.indexOf(tab)}
    /**
     * 
     * @param {EditorTab} tab to focus
     */
    setFocusedTab(tab) {
        let old = this.#focusedTab
        if (old) old.history = this.#editor.cm.getHistory()
        this.#focusedTab = tab
        this.emit('focused-tab-changed', old, tab)
    }

    /**
     * Set the content of the editor
     * @param {String} content 
     * @param {boolean} tabOpened true to disable the saving comportement & false to enable it
     */
    setContent(content, tabOpened = false) {
        this.#save = !tabOpened
        this.#editor.setMarkdown(content)
        this.#focusedTab && tabOpened && this.#editor.cm.setHistory(this.#focusedTab.history)
    }
    isOpened(path) {
        let result = this.#tabs.find((tab) => tab.getPath() === path);
        return result != undefined
    }

    getTabOf(path) {
        return this.#tabs.find((tab) => tab.getPath() === path);
    }

    hasUnsavedTabs() {
        for(const tab of this.#tabs) {
            if (!tab.isSaved()) return true
        }
        return false
    }
}

module.exports = EditorView