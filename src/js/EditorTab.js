const { readFileSync, writeFile, existsSync } = require('fs')
const basename = require('path').basename

class EditorTab {
    /**
     * @type {String}
     */
    #path;
    /**
     * @type {String}
     */
    #content;
    /**
     * @type {Number}
     */
    #id;

    /**
     * @type {Boolean}
     */
    #saved =true;
    /**
     * Undo/redo history
     */
    history = { done: [], undone: [] }
    /**
     * 
     * @param {String} path the path to the file to display
     * @param {Number} id id of the tab in the EditorView
     */
    constructor (path, id) {
        this.#path = path;
        this.#id = id;
    }

    async updateContentFromFile() {
        let data
        this.#saved = true
        if (existsSync(this.#path)) data = readFileSync(this.#path, {encoding: 'utf8'})
        else {
            data = ""
            this.#saved = false
        }

        if (data instanceof Error) throw data

        this.#content = data
        return data
    }

    /**
     * 
     * @param {String} data to put in content
     */
    updateContent(data) {
        if (data == this.#content) return;
        this.#content = data;
        this.#saved = false;
    }

    saveContent() {
        writeFile(this.#path, this.#content || "", 'utf8', () => {});
        this.#saved = true;
    }

    saveAs(path) {
        this.#path = path
        this.saveContent()
    }

    /**
     * @returns {String}
     */
    getName() {
        if (this.#path) return basename(this.#path).length > 12 ? basename(this.#path).slice(0, 10) + '...' : basename(this.#path)
        if (!this.#content) return "untitled"
        return this.#content.trim().split(/\r\n/gm)[0].slice(0, 12)
    }
    
    getPath() { return this.#path; }

    /**
     * 
     * @param {String} path the path to the file
     */
    setPath(path) { this.#path = path }

    getContent(getOrLoad = false) {
        if (!getOrLoad || this.#path === null) return this.#content;
        else {
            this.updateContentFromFile().then(() => this.#content)
        }
    }

    getId() { return this.#id }
    setId(id) { this.#id = id }

    isSaved() { return this.#saved }
}

module.exports = EditorTab