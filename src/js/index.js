const EditorView = require("./src/js/EditorView");
const ipc = require("electron").ipcRenderer;

/**
 * @type {EditorView}
 */
const view = new EditorView();

function updateTabs() {
  document.getElementById("nav").innerHTML = "";

  for (let tab of view.getTabs()) createTab(tab)

  if (view.getFocusedTab()) view.setContent(view.getFocusedTab().getContent(), true)
}

function updateTab(tab) {
  if (tab == null) return
  const tabElmt = document.getElementById(`tab-${tab.getId()}`);
  if (!tab.isSaved()) tabElmt.classList.add("unsaved-tab");
  else tabElmt.classList.remove("unsaved-tab");

  if (tab === view.getFocusedTab()) {
    tabElmt.classList.add("focused-tab")
    updateTitle(tab)
  }else tabElmt.classList.remove("focused-tab");

  tabElmt.innerHTML = `<div>${tab.getName()}</div><div class="tab-status close-file-icon" onclick="closeTabClicked(event, ${tab.getId()})"></div>`
}
/**
 * @param {EditorTab}
 */
function createTab(tab) {
  const tabElmt = document.createElement("div")

  tabElmt.innerHTML = `<div>${tab.getName()}</div><div class="tab-status close-file-icon" onclick="closeTabClicked(event, ${tab.getId()})"></div>`
  tabElmt.id = `tab-${tab.getId()}`
  tabElmt.classList = ["tab"]
  tabElmt.dataset.tabId = tab.getId()
  tabElmt.onclick = function () {
    view.setFocusedTab(tab)
  }

  if (!tab.isSaved()) tabElmt.classList.add("unsaved-tab")

  if (tab === view.getFocusedTab()) tabElmt.classList.add("focused-tab")

  document.getElementById("nav").appendChild(tabElmt)
}

function closeTabClicked(event, tabId) {
  event.stopPropagation()
  const tab = view.getTab(tabId)
  if (tab.isSaved()) view.removeTab(tab)
  else ipc.send("confirm-file-close", `File changed, save modifications?`, tabId)
}

/**
 * Change the app title or clear it if tab is null
 * @param tab EditorTab or null
 */
function updateTitle(tab) {
  if (tab) {
    ipc.send('update-app-title', `${tab.getPath() || tab.getName()}${tab.isSaved() ? '' : ' ●'}`)
    return;
  }
  ipc.send('update-app-title', null)
}

//events

view.on("content-updated", () => {
  const tab = view.getFocusedTab()
  updateTab(tab)
  ipc.send('has-unsaved-files', view.hasUnsavedTabs())
});

view.on("tab-created", tab => createTab(tab) || updateTitle(tab))

view.on("focused-tab-changed", (old, tab) => {
  if (old) updateTab(old);
  updateTab(tab);
  view.setContent(tab.getContent(), true)
})

view.on("tab-removed", (old, focusedTab) => {
  updateTabs()
  if (!focusedTab) view.setContent(null, true) || updateTitle()
  ipc.send('has-unsaved-files', view.hasUnsavedTabs())
});

//IPC
ipc.on("file-to-open", (event, path) => {
  if (view.isOpened(path)) {
    view.setFocusedTab(view.getTabOf(path));
  } else {
    let tab = view.createTab(path);
    tab.updateContentFromFile().then((data) => {
      view.setFocusedTab(tab);
    });
  }
});

ipc.on("save-focused-tab", (event, path, tabId) => {
  let tab = view.getTab(tabId)
  if (path) {
    if (tab) {
      tab.saveAs(path);
      updateTab(tab);
    } else {
      view.setFocusedTab(view.createTab(path));
      view.getFocusedTab().saveAs(path);
    }
  } else {
    tab = view.getFocusedTab()
    if (tab && tab.getPath()) {
      tab.saveContent();
      updateTab(tab);
    } else {
      ipc.send("save-as", false, tab ? tab.getId() : null);
    }
  }
  ipc.send('has-unsaved-files', view.hasUnsavedTabs())
});

ipc.on("new-tab", () => {
  view.setFocusedTab(view.createTab(null));
});

ipc.on("close-focused-tab", () => {
  if (!view.getFocusedTab()) {
    ipc.send("exit");
  } else if (view.getFocusedTab().isSaved()) {
    view.removeTab(view.getFocusedTab());
  } else ipc.send(
      "confirm-file-close",
      `File changed, save modifications?`
    );
});

ipc.on("confirm-file-close-reply", (event, index, tabId) => {
  switch (index) {
    case 0:
      if (!view.getFocusedTab().getPath()) ipc.send("save-as", true, tabId);
      else view.getFocusedTab().saveContent()
    case 1:
      view.removeTab(view.getFocusedTab());
      break;
    case 2:
      break;
  }
});

ipc.on("confirm-file-close", (event, path, tabId) => {
  if (path != undefined && tabId != undefined) {
    view.getTab(tabId).saveAs(path)
  }
  view.removeTab(view.getFocusedTab())
  ipc.send('has-unsaved-files', view.hasUnsavedTabs())
});

ipc.on("theme-changed", (event, area, theme) => {
  switch (area) {
    case "general":
      view.getEditor().setTheme(theme);
      document.documentElement.setAttribute("theme", theme);
      localStorage.setItem("mdEditorThemeGeneral", theme);
      break;

    case "editor":
      view.getEditor().setEditorTheme(theme);
      localStorage.setItem("mdEditorThemeEditor", theme);
      break;

    case "preview":
      view.getEditor().setPreviewTheme(theme);
      localStorage.setItem("mdEditorThemePreview", theme);
      break;

    default:
      view.getEditor().setTheme("default");
      view.getEditor().setEditorTheme("default");
      view.getEditor().setPreviewTheme("default");
      document.documentElement.setAttribute("theme", "light");
  }
});

ipc.on('save-all-files-reply', (event, index) => {
  if (index === 0) {
    for(const tab of view.getTabs()) {
      if (tab.getPath() != null) tab.saveContent()
      else {
        ipc.send("save-as", false, tab.getId())
      }
    }
  }
})
