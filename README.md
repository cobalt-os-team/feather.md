# Feather.md

Feather.md is a Windows/Linux/MacOS application to edit markdown files with ease.
It uses [MeditorJS](https://gitlab.com/cobalt-os-team/MeditorJS) to render the markdown.

![preview](build/interface.png)

## How to use

You can download the installer from the [releases](https://gitlab.com/cobalt-os-team/feather.md/-/releases). Note that if you are on MacOS you will have to compile the application on your own.

## Compilation

### Dependencies to compile

To compile Feather.md you will need npm.

In ArchLinux you can install npm using `pacman`:

```sh 
sudo pacman -S npm
```

In MacOS you can install npm using `brew`:

```sh 
brew install npm 
```

Optionally, yarn can be used to install the JavaScript dependencies.

You can install yarn the same way you installed npm, or you could use npm:

```sh
npm install --global yarn
```

### Compiling

In the root of this repository follow these steps:

1. Install the JavaScript dependencies with npm:

```sh 
npm install
```

Or with yarn:

```sh 
yarn install
```

2. Compile with npm

You can compile Feather.md for multiple operating system changing the target of npm.

- `dist-win`: Generate the Windows binary.
- `dist-mac`: Generate the MacOS binary.
- `dist-linux`: Generate the Linux binary.
- `dist-all`: Generate binaries for all systems.

```sh
npm run dist-all
```

The output will be generated in the folder `dist`.

